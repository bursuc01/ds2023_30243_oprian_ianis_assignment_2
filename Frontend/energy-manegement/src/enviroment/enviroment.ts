export const enviroment = {
    production: false,
    API_USER_BASE_URL: 'https://localhost:7006',
    //API_USER_BASE_URL: 'https://localhost:7006',
    API_DEVICE_BASE_URL: 'http://localhost:5267',
    //API_DEVICE_BASE_URL: 'http://localhost:5267'
    WS_MONITOR_BASE_URL: 'wss://localhost:7136'
}