export interface UserCreate {
    name: string;
    password: string;
    isAdmin: boolean;
  }