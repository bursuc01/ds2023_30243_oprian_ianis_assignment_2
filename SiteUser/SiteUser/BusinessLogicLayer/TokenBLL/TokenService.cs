using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using SiteUser.DataLayer.Models;

namespace SiteUser.BusinessLogicLayer.TokenBLL;

public class TokenService : ITokenService
{
    public JwtSecurityToken CreateTokenOptions(User user)
    {
        var claims = new[]
        {
            new Claim("Id", user.Id.ToString()),
            new Claim("Name", user.Name),
            new Claim("IsAdmin", user.IsAdmin.ToString()),
        };
            
        var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Generate256BitsKey()));
        var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
        var tokeOptions = new JwtSecurityToken(
            issuer: "https://localhost:5001",
            audience: "https://localhost:5001",
            claims: claims,
            expires: DateTime.Now.AddMinutes(5),
            signingCredentials: signinCredentials
        );

        return tokeOptions;
    }

    public JwtSecurityToken CreateTokenOption()
    {
        var secretKey = new SymmetricSecurityKey("superSecretKey@345"u8.ToArray());
        var signinCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);
        var tokenOption = new JwtSecurityToken(
            issuer: "https://localhost:5001",
            audience: "https://localhost:5001",
            claims: new List<Claim>(),
            expires: DateTime.Now.AddMinutes(5),
            signingCredentials: signinCredentials
        );

        return tokenOption;
    }

    private static string Generate256BitsKey()
    {
        using var cryptoProvider = new RNGCryptoServiceProvider();
        var secretKeyByteArray = new byte[32]; // 256 bits
        cryptoProvider.GetBytes(secretKeyByteArray);
        return Convert.ToBase64String(secretKeyByteArray);
    }
}